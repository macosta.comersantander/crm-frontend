// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: ['@sidebase/nuxt-auth','@nuxtjs/apollo'],
  apollo: {
    clients: {
      default: {
        httpEndpoint: 'http://localhost:8000/graphql',
        tokenStorage: 'cookie',
        tokenName: 'auth:token',
      }
    },
  },
  auth: {
    baseURL: 'http://localhost:8000/',
    provider: {
        type: 'local',
        endpoints: {
          signIn: { path: '/auth', method: 'post' },
          getSession: { path: '/usertoken', method: 'get' },
        },
    },
  },
  app: {
    head: {
      bodyAttrs: {
        class: 'skin-default-dark fixed-layout',
      },
      link: [{ rel: 'stylesheet', href: '/css/style.min.css' }],
      script: [
        { src: '/js/jquery.min.js' },
        { src: '/js/bootstrap.bundle.min.js' },
        { src: '/js/perfect-scrollbar.jquery.min.js' },
        { src: '/js/waves.js' },
        { src: '/js/sidebarmenu.js' },
        { src: '/js/custom.min.js' },
      ],
    },
  },
  runtimeConfig: {
    // The private keys which are only available within server-side
    apiSecret: '123',
    // Keys within public, will be also exposed to the client-side
    public: {
      apiBase: '/api',
    },
  }
})
